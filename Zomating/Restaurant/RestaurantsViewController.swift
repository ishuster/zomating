//
//  RestaurantsViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

// Note: Even though linter is giving a warning, `Searchable` declares it is for `AnyObject`
// By using `AnyObject` here, compiler will tell us that is redundant.
protocol RestaurantsViewControllerDelegate: Searchable {
    func restaurantsViewController(_ viewController: RestaurantsViewController, didSelect restaurant: Restaurant)
}

final class RestaurantsViewController: BaseViewController {
    
    // MARK: - Public Properties
    // MARK: -
    weak var delegate: RestaurantsViewControllerDelegate?
    
    // MARK: - Private Properties
    // MARK: -
    private let networkClient: NetworkClient
    private let category: Category
    private var dataSource = RepositoryDataSource<Restaurant>()
    
    private lazy var restaurantsView: RestaurantsView = {
        let view = RestaurantsView()
        view.tableView.delegate = self
        view.tableView.dataSource = self
        view.tableView.register(cellType: RestaurantTableViewCell.self)
        view.tableView.rowHeight = UITableView.automaticDimension
        view.tableView.estimatedRowHeight = 200
        view.tableView.separatorStyle = .none
        view.tableView.backgroundColor = .systemGray
        return view
    }()
    
    // MARK: - View Controller Life Cycle
    // MARK: -
    init(category: Category, networkClient: NetworkClient) {
        self.networkClient = networkClient
        self.category = category
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = restaurantsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyView.delegate = self
        navigationItem.title = category.name
        requestRestaurants()
    }
    
    private func requestRestaurants() {
        startAnimatingActivityIndicator()
        networkClient.requestRestaurants(by: category) { [weak self] (result) in
            guard let self = self else { return }
            self.stopAnimatingActivityIndicator()
            switch result {
            case .success(let restaurants):
                self.dataSource.update(newItems: restaurants)
                let indexPaths = self.dataSource.itemsIndexPaths()
                DispatchQueue.main.async {
                    if self.isViewLoaded {
                        self.restaurantsView.tableView.insertRows(at: indexPaths, with: .automatic)
                        self.restaurantsView.tableView.reloadData()
                    }
                }
                
            case .failure(let error):
                print(error)
                let alertController = UIAlertController(title: "Network Error", message: error.localizedDescription, handler: { (action) in
                    print(action)
                })
                
                DispatchQueue.main.async {
                    self.present(alertController, animated: true)
                }
            }
        }
    }
    
    // MARK: - Actions
    // MARK: -
    override func searchTouchUpInside(_ sender: Any) {
        delegate?.viewControllerWillGoToSearch(self)
    }
}

// MARK: - EmptyViewDelegate
// MARK: -
extension RestaurantsViewController: EmptyViewDelegate {
    func emptyView(_ view: EmptyView, didSelect retryButton: UIButton) {
        requestRestaurants()
    }
}

// MARK: - UITableViewDelegate
// MARK: -
extension RestaurantsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let restaurant = dataSource.item(at: indexPath) else { return }
        delegate?.restaurantsViewController(self, didSelect: restaurant)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

// MARK: - UITableViewDataSource
// MARK: -
extension RestaurantsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = dataSource.numberOfItems(forSection: section)
        if numberOfRows == 0 {
            showEmptyView()
        } else {
            hideEmptyView()
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(for: indexPath) as RestaurantTableViewCell
        guard let restaurant = dataSource.item(at: indexPath) else { return cell }
        
        cell.name = restaurant.name
        cell.votes = restaurant.userRating.votes
        cell.rate = restaurant.userRating.aggregateRating ?? "—"
        cell.thumbUrlString = restaurant.thumb
        cell.timings = restaurant.timings
        cell.cuisine = restaurant.cuisines
        cell.location = restaurant.location.localityVerbose
        
        return cell
    }
}
