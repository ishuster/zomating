//
//  RestaurantDetailViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol RestaurantDetailViewControllerDelegate: Searchable {
    func restaurantDetailViewController(_ viewController: RestaurantDetailViewController, willShow allReviews: AllReviews)
    func restaurantDetailViewController(_ viewController: RestaurantDetailViewController, photoElements: [PhotoElement], selected index: Int)
}

final class RestaurantDetailViewController: BaseViewController {
    
    // MARK: - Public Properties
    // MARK: -
    weak var delegate: RestaurantDetailViewControllerDelegate?
    
    // MARK: - Private Properties
    // MARK: -
    private let restaurant: Restaurant
    private let networkClient: NetworkClient
    private lazy var galleryViewController: GalleryViewController = {
       let viewController = GalleryViewController(photoElements: restaurant.photos ?? [])
        viewController.delegate = self
        return viewController
    }()
    
    private lazy var detailView: RestaurantsDetailView = {
        let view = RestaurantsDetailView(frame: .zero)
        return view
    }()
    
    init(restaurant: Restaurant, networkClient: NetworkClient) {
        self.restaurant = restaurant
        self.networkClient = networkClient
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = detailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = restaurant.name
        detailView.imageUrlString = restaurant.featuredImage
        detailView.name = restaurant.name
        detailView.cuisine = restaurant.cuisines
        detailView.location = restaurant.location.localityVerbose
        detailView.hours = restaurant.timings
        detailView.votes = restaurant.userRating.votes
        detailView.aggregateRating = restaurant.userRating.aggregateRating ?? "—"
        detailView.rating = restaurant.userRating.ratingText
        detailView.reviewCount = "\(restaurant.allReviews.reviews.count)"
        if let firstReview = restaurant.allReviews.reviews.first?.review {
            detailView.review(withText: firstReview.reviewText,
                              reviewTimeFriendly: firstReview.reviewTimeFriendly,
                              ratingText: firstReview.ratingText,
                              username: firstReview.user.name)
        }
        
        detailView.allReviewsTouchUpInside(selector: #selector(seeAllReviewsTouchUpInside(_:)), target: self)
        addChild(galleryViewController)
        galleryViewController.didMove(toParent: self)
        detailView.insertPhotoGalleryView(galleryViewController.view)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        detailView.didUpdateLayout()
    }
    
    // MARK: - Actions
    // MARK: -
    override func searchTouchUpInside(_ sender: Any) {
        delegate?.viewControllerWillGoToSearch(self)
    }
    
    @objc private func seeAllReviewsTouchUpInside(_ button: UIButton) {
        delegate?.restaurantDetailViewController(self, willShow: restaurant.allReviews)
    }
}

extension RestaurantDetailViewController: GalleryViewControllerDelegate {
    func galleryViewController(_ viewController: GalleryViewController, didSelect photo: Photo, at index: Int, photoElements: [PhotoElement]) {
        delegate?.restaurantDetailViewController(self, photoElements: photoElements, selected: index)
    }
}
