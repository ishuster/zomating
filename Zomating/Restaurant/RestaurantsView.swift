//
//  RestaurantsView.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/16/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class RestaurantsView: UIView {

    let tableView = UITableView(frame: .zero, style: .plain)
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(tableView)
        tableView.pinToEdges().activate()
    }

}
