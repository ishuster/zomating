//
//  RestaurantTableViewCell.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class RestaurantTableViewCell: UITableViewCell {
    
    // MARK: - Public Properties
    // MARK: -
    var name: String = "Restaurant name" {
        didSet {
            nameLabel.text = name
        }
    }
    
    var votes: String = "0" {
        didSet {
            votesLabel.text = votes + " votes"
        }
    }
    
    var rate: String = "0" {
        didSet {
            rateLabel.text = rate + "/5"
        }
    }
    
    var thumbUrlString: String = "" {
        didSet {
            thumbImageView.loadImage(urlString: thumbUrlString)
        }
    }
    
    var cuisine: String = "" {
        didSet {
            cousineLabel.text = cuisine
        }
    }
    
    var location: String = "" {
        didSet {
            locationLabel.text = location
        }
    }
    
    var timings: String = "" {
        didSet {
            timingsLabel.text = timings
        }
    }
    
    // MARK: - Private Properties
    // MARK: -
    private let thumbImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        return imageView
    }()
    
    private let nameLabel: UILabel = {
        let label: UILabel = .create(text: "Restaurant name")
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        label.setContentHuggingPriority(.init(rawValue: 750), for: .vertical)
        label.setContentCompressionResistancePriority(.init(rawValue: 750), for: .vertical)
        return label
    }()
    
    private let rateLabel: UILabel = {
        let label = UILabel.create(text: "4.5/5")
        label.textColor = .black
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        return label
    }()
    
    private let votesLabel: UILabel = {
        let label = UILabel.create(text: "555 votes")
        label.textColor = .black
        label.textAlignment = .right
        label.font = UIFont.italicSystemFont(ofSize: 13)
        return label
    }()
    
    private let cousineLabel: UILabel = {
        let label: UILabel = .create(text: "Cuisine")
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.setContentHuggingPriority(.init(rawValue: 740), for: .vertical)
        label.setContentCompressionResistancePriority(.init(rawValue: 740), for: .vertical)
        return label
    }()
    
    private let locationLabel: UILabel = {
        let label: UILabel = .create(text: "Location")
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.setContentHuggingPriority(.init(rawValue: 730), for: .vertical)
        label.setContentCompressionResistancePriority(.init(rawValue: 730), for: .vertical)
        return label
    }()
    
    private let timingsLabel: UILabel = {
        let label: UILabel = .create(text: "Timings")
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.setContentHuggingPriority(.init(rawValue: 720), for: .vertical)
        label.setContentCompressionResistancePriority(.init(rawValue: 720), for: .vertical)
        return label
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 4
        return stackView
    }()
    
    private lazy var rateStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [rateLabel, votesLabel])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 4
        return stackView
    }()
    
    private lazy var nameAndRateStackView: UIStackView = {
        let nameAndRateStackView = UIStackView(arrangedSubviews: [nameLabel, rateStackView])
        nameAndRateStackView.axis = .horizontal
        nameAndRateStackView.distribution = .fillEqually
        return nameAndRateStackView
    }()
    
    private let containerView = UIView()

    // MARK: - Life cycle
    // MARK: -
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        contentView.addSubview(containerView)
        applyShadow()

        [thumbImageView, stackView].forEach { containerView.addSubview($0) }
        [nameAndRateStackView, cousineLabel, locationLabel, timingsLabel].forEach { stackView.addArrangedSubview($0) }
        
        let padding: CGFloat = 8
        containerView.pinToEdges(insets: UIEdgeInsets(top: padding, left: padding, bottom: -padding, right: -padding)).activate()
        thumbImageView.leftToSuperView(offset: padding, activate: true)
        thumbImageView.bottom(to: stackView, activate: true)
        thumbImageView.top(to: stackView, activate: true)
        thumbImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        stackView.leftToRight(of: thumbImageView, offset: padding, activate: true)
        stackView.topToSuperview(offset: padding, activate: true)
        stackView.rightToSuperView(offset: -padding, activate: true)
        stackView.bottomToSuperview(offset: -padding, activate: true)
        
    }
    
    private func applyShadow() {
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 4
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.backgroundColor = .white
    }
    
}
