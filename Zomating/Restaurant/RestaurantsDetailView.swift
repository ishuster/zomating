//
//  RestaurantsDetailView.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class RestaurantsDetailView: UIView {
    
    // MARK: - Public Properties
    // MARK: -
    var imageUrlString: String = "" {
        didSet {
            imageView.loadImage(urlString: imageUrlString)
        }
    }
    
    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }
    
    var cuisine: String = "" {
        didSet {
            cuisineLabel.text = cuisine
        }
    }
    
    var location: String = "" {
        didSet {
            locationLabel.text = "in " + location
        }
    }
    
    var hours: String = "" {
        didSet {
            hoursLabel.text = hours
        }
    }
    
    var votes: String = "" {
        didSet {
            votesLabel.text = votes + " votes"
        }
    }
    
    var aggregateRating: String = "0" {
        didSet {
            aggregateRatingLabel.text = aggregateRating + "/5"
        }
    }
    
    var rating: String = "" {
        didSet {
            ratingLabel.text = rating
        }
    }
    
    var ratingColor: String = "" {
        didSet {
            ratingLabel.textColor = .black
        }
    }
    
    var reviewCount: String = "0" {
        didSet {
            seeAllReviewsButton.setTitle("See all reviews (\(reviewCount))", for: .normal)
        }
    }

    // MARK: - Private Properties
    // MARK: -
    private static let cellHeightConstant: CGFloat = 40
    let scrollView = ArrangedScrollView()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let nameLabel: Label = {
        let label = Label(insets: .label, text: "Restaurant name")
        label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        label.backgroundColor = .white
        return label
    }()
    
    private let cuisineLabel: Label = {
        let label = Label(insets: .label, text: "Cuisine")
        label.font = UIFont.systemFont(ofSize: 13, weight: .light)
        label.font = UIFont.italicSystemFont(ofSize: 13)
        label.backgroundColor = .white
        label.textColor = .lightGray
        return label
    }()
    
    private let locationLabel: Label = {
        let label = Label(insets: .label, text: "Location")
        label.font = UIFont.systemFont(ofSize: 13, weight: .light)
        label.font = UIFont.italicSystemFont(ofSize: 13)
        label.backgroundColor = .white
        label.textColor = .lightGray
        return label
    }()
    
    private let hoursLabel: Label = {
        let label = Label(insets: .label, text: "Hours", heightConstant: cellHeightConstant)
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.backgroundColor = .white
        return label
    }()
    
    private let votesLabel: Label = {
        let label = Label(insets: .label, text: "Hours", heightConstant: cellHeightConstant)
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.backgroundColor = .white
        return label
    }()
    
    private let aggregateRatingLabel: Label = {
        let label = Label(insets: .label, text: "0/5", heightConstant: cellHeightConstant)
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.backgroundColor = .white
        return label
    }()
    
    private let ratingLabel: Label = {
        let label = Label(insets: .label, text: "0/5", heightConstant: cellHeightConstant)
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.backgroundColor = .white
        return label
    }()
    
    private let firstReviewLabel: Label = {
        let label = Label(insets: .label, text: "———")
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.backgroundColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    private let seeAllReviewsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.black, for: .normal)
        button.setTitle("See all reviews (0)", for: .normal)
        button.backgroundColor = .white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        return button
    }()
    
    private let photosContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.heightAnchor.constraint(equalToConstant: 150).isActive = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .systemGray
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(scrollView)
        scrollView.clipsToBounds = true
        scrollView.pinToEdges(safeArea: true).activate()
    
        // Hours
        let hoursTitleLabel = createSectionTitleLabel(text: "Hours")

        scrollView.addArranged(subviews: imageView, nameLabel, cuisineLabel, locationLabel, hoursTitleLabel, hoursLabel)
        // Image view
        imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.33).isActive = true
        
        // Spacing
        scrollView.stackView.setCustomSpacing(1, after: imageView)
        scrollView.stackView.setCustomSpacing(10, after: locationLabel)
        
        // Rating
        let ratingTitleLabel = createSectionTitleLabel(text: "Rating")
        let ratingSubivews = [ratingTitleLabel, ratingLabel, aggregateRatingLabel, votesLabel]
        scrollView.addArranged(subviews: ratingSubivews)
        scrollView.stackView.setCustomSpacing(10, after: hoursLabel)
        scrollView.stackView.setCustomSpacing(5, after: ratingTitleLabel)
        ratingSubivews.forEach { scrollView.stackView.setCustomSpacing(1, after: $0) }
        
        // Review
        scrollView.stackView.setCustomSpacing(10, after: votesLabel)
        let reviewsTitleLabel = createSectionTitleLabel(text: "Reviews")
        let reviewSubviews = [reviewsTitleLabel, firstReviewLabel, seeAllReviewsButton]
        scrollView.addArranged(subviews: reviewSubviews)
        reviewSubviews.forEach { scrollView.stackView.setCustomSpacing(1, after: $0) }
        
        // Photos
        let photosTitleLabel = createSectionTitleLabel(text: "Photos")
        let photosSubviews = [photosTitleLabel, photosContainerView]
        scrollView.addArranged(subviews: photosSubviews)
        scrollView.stackView.setCustomSpacing(10, after: seeAllReviewsButton)
    }
    
    private func createSectionTitleLabel(text: String) -> Label {
        let label = Label(insets: .label, text: text)
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        label.backgroundColor = .systemGray
        return label
    }
    
    // MARK: - Public methods
    // MARK: -
    func review(withText text: String, reviewTimeFriendly: String, ratingText: String, username: String) {
        var reviewText =
        """
        
        \(ratingText) by \(username)
        \(reviewTimeFriendly)
        
        """
        
        if !text.isEmpty {
            reviewText += "\n\(text)\n"
        }
        firstReviewLabel.text = reviewText
    }
    
    func allReviewsTouchUpInside(selector: Selector, target: Any) {
        seeAllReviewsButton.addTarget(target, action: selector, for: .touchUpInside)
    }
    
    func didUpdateLayout() {
        scrollView.didUpdateLayout()
    }
    
    func insertPhotoGalleryView(_ view: UIView) {
        photosContainerView.addSubview(view)
        view.pinToEdges().activate()
    }
}
