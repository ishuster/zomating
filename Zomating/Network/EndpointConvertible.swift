//
//  EndpointConvertible.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol EndpointConvertible {
    var baseURLString: String { get }
    var path: String { get }
    var httpMethod: HttpMethod { get }
    var userKey: String { get }
    
    func toURL() throws -> URL
    func toURL(params: [String: Any]) throws -> URL
}

extension EndpointConvertible {
    func toURL() throws -> URL {
        guard var url = URL(string: baseURLString) else {
            throw EndpointError.invalidURL
        }
        url.appendPathComponent(path)
        return url
    }
    
    func toURL(params: [String: Any]) throws -> URL {
        guard let parameters = params as? [String: String] else {
            throw EndpointError.invalidParameters
        }
        
        let encodedParams = parameters.toURLEncodedString()
        guard let url = URL(string: "\(baseURLString)/\(path)?\(encodedParams)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
}

extension EndpointConvertible {
    var baseURLString: String {
        return Environment.baseURLString
    }
    
    var userKey: String {
        return Environment.userKey
    }
}
