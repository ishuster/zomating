//
//  NetworkClient.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class NetworkClient {
    
    let urlSession: URLSession
    
    private lazy var cache = NSCache<NSString, NSData>()
    private let operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "Network client operations"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    func requestCategories(completion: @escaping (Result<[Category], Error>) -> Void) {
        let op = CategoriesOperation(session: urlSession) { (result: Result<CategoriesWrapper, Error>) in
            switch result {
            case .success(let value):
                let categories = value.categories.compactMap { $0 }.compactMap { $0.categories }
                completion(.success(categories))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(op)
    }
    
    func requestRestaurants(by category: Category, completion: @escaping (Result<[Restaurant], Error>) -> Void) {
        let op = SearchOperation(category: category, session: urlSession) { (result: Result<SearchByCategory, Error>) in
            switch result {
            case .success(let value):
                let restaurants = value.restaurants.compactMap { $0.restaurant }
                completion(.success(restaurants))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(op)
    }
    
    func search(with text: String, completion: @escaping (Result<[Restaurant], Error>) -> Void) {
        
        if let cachedData = cache.object(forKey: text as NSString),
            let searchByCategory = try? JSONDecoder().decode(SearchByCategory.self, from: cachedData as Data) {
            
            print("👀 returning data from cache for key: \(text)")
            let restaurants = searchByCategory.restaurants.compactMap { $0.restaurant }
            completion(.success(restaurants))
            return
        }
        
        let operation = SearchQueryOperation(query: text, session: urlSession) { [weak self] (result: Result<SearchByCategory, Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let value):
                let restaurants = value.restaurants.compactMap { $0.restaurant }
                
                if let data = try? JSONEncoder().encode(value) {
                    print("🦄 storing data in cache for key: \(text)")
                    self.cache.setObject(data as NSData, forKey: text as NSString)
                }
                
                completion(.success(restaurants))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }
}
