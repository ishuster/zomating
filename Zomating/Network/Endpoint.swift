//
//  Endpoint.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum Endpoint: EndpointConvertible {
    case categories
    case search
    var path: String {
        switch self {
        case .categories:
            return "/categories"
            
        case .search:
            return "/search"
        }
    }
    
    var httpMethod: HttpMethod {
        return .get
    }
}
