//
//  CategoriesOperation.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class CategoriesOperation: BaseNetworkOperation<CategoriesWrapper> {
    override var endpoint: EndpointConvertible {
        return Endpoint.categories
    }
}

final class SearchOperation: BaseNetworkOperation<SearchByCategory> {
    let category: Category
    
    override var endpoint: EndpointConvertible {
        return Endpoint.search
    }
    
    override var parameters: [String: Any]? {
        return ["category": "\(category.id)", "sort": "rating", "order": "desc"]
    }
    
    init(category: Category, session: URLSession = URLSession.shared, completion: @escaping Completion) {
        self.category = category
        super.init(session: session, completion: completion)
    }
}

final class SearchQueryOperation: BaseNetworkOperation<SearchByCategory> {
    let query: String
    
    override var endpoint: EndpointConvertible {
        return Endpoint.search
    }
    
    override var parameters: [String: Any]? {
        return ["q": query]
    }
    
    init(query: String, session: URLSession = URLSession.shared, completion: @escaping Completion) {
        self.query = query
        super.init(session: session, completion: completion)
    }
}
