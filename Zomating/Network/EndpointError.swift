//
//  EndpointError.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum EndpointError: Error {
    case invalidURL
    case invalidParameters
}
