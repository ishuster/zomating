//
//  ReviewsViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class ReviewsViewController: UIViewController {

    private let allReviews: AllReviews
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(cellType: ReviewTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    private lazy var dataSource = RepositoryDataSource<Review>()
    
    init(allReviews: AllReviews) {
        self.allReviews = allReviews
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = tableView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Reviews"
        let reviews = allReviews.reviews.compactMap { $0.review }
        dataSource.update(newItems: reviews)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.reloadData()
    }
}

extension ReviewsViewController: UITableViewDelegate {
    
}

extension ReviewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfItems(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(for: indexPath) as ReviewTableViewCell
        guard let review = dataSource.item(at: indexPath) else { return cell }
        cell.textLabel?.text = "\(review.ratingText) by \(review.user.name)"
        cell.detailTextLabel?.text = "\(review.reviewTimeFriendly) \n\n\(review.reviewText)"
        cell.selectionStyle = .none
        return cell
    }
}
