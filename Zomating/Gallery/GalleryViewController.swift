//
//  GalleryViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol GalleryViewControllerDelegate: AnyObject {
    func galleryViewController(_ viewController: GalleryViewController, didSelect photo: Photo, at index: Int, photoElements: [PhotoElement])
}

final class GalleryViewController: UIViewController {
    
    var selectedIndex: Int?
    weak var delegate: GalleryViewControllerDelegate?
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cellType: ImageCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .white
        return collectionView
    }()
    
    private var dataSource = RepositoryDataSource<Photo>()
    private let photoElements: [PhotoElement]
    private var isPagingEnabled = false
    
    init(photoElements: [PhotoElement], isPagingEnabled: Bool = false) {
        self.photoElements = photoElements
        super.init(nibName: nil, bundle: nil)
        self.isPagingEnabled = isPagingEnabled
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let photos = photoElements.compactMap { $0.photo }
        dataSource.update(newItems: photos)

        if isPagingEnabled, let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = .zero
        }
        
        collectionView.isPagingEnabled = isPagingEnabled
        collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectedIndex = selectedIndex {
            let indexPath = IndexPath(row: selectedIndex, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}

// MARK: - UICollectionViewDataSource
// MARK: -
extension GalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfItems(forSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(for: indexPath) as ImageCollectionViewCell
        guard let photo = dataSource.item(at: indexPath) else { return cell }
        cell.loadUrl(photo.url)
        if isPagingEnabled {
            cell.customContentMode = .scaleAspectFit
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate
// MARK: -
extension GalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let photo = dataSource.item(at: indexPath) else { return }
        delegate?.galleryViewController(self, didSelect: photo, at: indexPath.row, photoElements: photoElements)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
// MARK: -
extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isPagingEnabled {
            return CGSize(width: view.frame.width, height: view.frame.height)
        }
        
        var padding: CGFloat = 40
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = (layout.sectionInset.left + layout.sectionInset.right) * 1.5
        }
        let width = (collectionView.frame.width - padding) / 3
        return CGSize(width: width, height: collectionView.frame.height)
    }
}
