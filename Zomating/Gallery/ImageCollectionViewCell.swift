//
//  ImageCollectionViewCell.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class ImageCollectionViewCell: UICollectionViewCell {
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5.0
        return imageView
    }()
    
    var customContentMode: UIView.ContentMode = .scaleAspectFill {
        didSet {
            imageView.contentMode = customContentMode
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        contentView.addSubview(imageView)
        imageView.pinToEdges(insets: UIEdgeInsets(top: 20, left: 8, bottom: -20, right: 8)).activate()
    }
    
    func loadUrl(_ urlString: String) {
        imageView.loadImage(urlString: urlString)
    }
}
