//
//  Repository.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol Repository {
    associatedtype Model
    
    var items: [[Model]] { get set }
    
    mutating func update(newItems: [Model], forSection section: Int)
    func item(at indexPath: IndexPath) -> Model?
    func numberOfItems(forSection section: Int) -> Int
}

extension Repository {
    
    func item(at indexPath: IndexPath) -> Model? {
        return items[indexPath.section][indexPath.row]
    }
    
    func numberOfItems(forSection section: Int) -> Int {
        if items.isEmpty {
            return 0
        }
        return items[section].count
    }
    
    mutating func update(newItems: [Model], forSection section: Int = 0) {
        if items.isEmpty {
            items.append(newItems)
        } else {
            if items.count > section {
                items[section] = newItems
            }
        }
    }
    
    func itemsIndexPaths() -> [IndexPath] {
        var indexPaths = [IndexPath]()
        for (section, elements) in items.enumerated() {
            for (row, _) in elements.enumerated() {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
            }
        }
        return indexPaths
    }
}
