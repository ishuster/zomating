//
//  Emptyable.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol Emptyable {
    var emptyView: EmptyView { get }
    func showEmptyView()
    func hideEmptyView()
}

extension Emptyable where Self: UIViewController {
    func showEmptyView() {
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.removeFromSuperview()
        
        view.addSubview(emptyView)
        emptyView.top(to: view).isActive = true
        emptyView.left(to: view).isActive = true
        emptyView.bottom(to: view).isActive = true
        emptyView.right(to: view).isActive = true
    }
    
    func hideEmptyView() {
        emptyView.removeFromSuperview()
    }
}
