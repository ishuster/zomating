//
//  Loadable.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol Loadable: AnyObject {
    var activityIndicator: UIActivityIndicatorView { get }
    var rootView: UIView { get }
}

extension Loadable where Self: UIViewController {
    var rootView: UIView {
        return tabBarController?.view ?? navigationController?.view ?? self.view
    }
    
    func startAnimatingActivityIndicator() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        DispatchQueue.main.async {
            self.activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            if let superView = self.activityIndicator.superview {
                self.activityIndicator.startAnimating()
                superView.bringSubviewToFront(self.activityIndicator)
            } else {
                self.rootView.addSubview(self.activityIndicator)
                self.activityIndicator.pinToEdges().activate()
                self.activityIndicator.startAnimating()
            }
        }
    }
    
    func stopAnimatingActivityIndicator() {
        UIView.animate(withDuration: 0.5, animations: { [weak activityIndicator] in
            DispatchQueue.main.async {
                activityIndicator?.stopAnimating()
            }
            
            }, completion: { [weak activityIndicator] _ in
                activityIndicator?.removeFromSuperview()
        })
    }
}
