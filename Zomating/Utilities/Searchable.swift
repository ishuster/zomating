//
//  Searchable.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/16/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol Searchable: AnyObject {
    func viewControllerWillGoToSearch(_ viewController: UIViewController)
}
