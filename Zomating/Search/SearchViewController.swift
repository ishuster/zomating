//
//  SearchViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/16/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate: AnyObject {
    func searchViewController(_ viewController: SearchViewController, didSelect restaurant: Restaurant)
}

final class SearchViewController: UIViewController, Emptyable {
    
    // MARK: - Public Properties
    // MARK: -
    let emptyView = EmptyView(frame: .zero)
    weak var delegate: SearchViewControllerDelegate?
    
    // MARK: - Private Properties
    // MARK: -
    private let tableView = UITableView(frame: .zero, style: .plain)
    private let networkClient: NetworkClient
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        return searchController
    }()
    
    private var dataSource = RepositoryDataSource<Restaurant>()
    private var searchWorkItem: DispatchWorkItem?
    
    // MARK: - View Controller Life Cycle
    // MARK: -
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.pinToEdges().activate()
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        navigationItem.title = "Search"
        tableView.register(cellType: RestaurantTableViewCell.self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.isActive = true
    }
    
    // MARK: - Network requests
    // MARK: -
    private func search(with text: String) {
        networkClient.search(with: text) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let restaurants):
                self.dataSource.update(newItems: restaurants)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            case .failure(let error):
                print(error)
                let alertController = UIAlertController(title: "Network Error", message: error.localizedDescription, handler: { (action) in
                    print(action)
                })
                
                DispatchQueue.main.async {
                    self.present(alertController, animated: true)
                }
            }
        }
    }
}

// MARK: - UITableViewDataSource
// MARK: -
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = dataSource.numberOfItems(forSection: section)
        numberOfRows > 0 ? hideEmptyView() : showEmptyView()
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(for: indexPath) as RestaurantTableViewCell
        guard let restaurant = dataSource.item(at: indexPath) else { return cell }
        
        cell.name = restaurant.name
        cell.votes = restaurant.userRating.votes
        cell.rate = restaurant.userRating.aggregateRating ?? "—"
        cell.thumbUrlString = restaurant.thumb
        cell.timings = restaurant.timings
        cell.cuisine = restaurant.cuisines
        cell.location = restaurant.location.localityVerbose
        
        return cell
    }
}

// MARK: - UITableViewDelegate
// MARK: -
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let restaurant = dataSource.item(at: indexPath) else { return }
        delegate?.searchViewController(self, didSelect: restaurant)
    }
}

// MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        if text.isEmpty {
            return
        }
        
        searchWorkItem?.cancel()
        
        let workItem = DispatchWorkItem(block: { [weak self] in
            guard let self = self else { return }
            self.search(with: text)
        })
        searchWorkItem = workItem
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: workItem)
    }
}
