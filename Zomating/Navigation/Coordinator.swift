//
//  Coordinator.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class Coordinator {
    let navigationController: UINavigationController
    let networkClient: NetworkClient
    
    init(navigationController: UINavigationController, networkClient: NetworkClient) {
        self.navigationController = navigationController
        navigationController.navigationBar.prefersLargeTitles = true
        self.networkClient = networkClient
    }
    
    func start() {
        let categoriesViewController = CategoriesViewController(networkClient: networkClient)
        categoriesViewController.delegate = self
        navigationController.pushViewController(categoriesViewController, animated: false)
    }
}

// MARK: - CategoriesViewControllerDelegate
// MARK: -
extension Coordinator: CategoriesViewControllerDelegate {
    func categoriesViewController(_ viewController: CategoriesViewController, didSelect category: Category) {
        let restaurantsViewController = RestaurantsViewController(category: category, networkClient: networkClient)
        restaurantsViewController.delegate = self
        navigationController.pushViewController(restaurantsViewController, animated: true)
    }
}

// MARK: - RestaurantsViewControllerDelegate
// MARK: -
extension Coordinator: RestaurantsViewControllerDelegate {
    func restaurantsViewController(_ viewController: RestaurantsViewController, didSelect restaurant: Restaurant) {
        let restaurantDetailViewController = RestaurantDetailViewController(restaurant: restaurant, networkClient: networkClient)
        restaurantDetailViewController.delegate = self
        navigationController.pushViewController(restaurantDetailViewController, animated: true)
    }
}

// MARK: - RestaurantDetailViewControllerDelegate
// MARK: -
extension Coordinator: RestaurantDetailViewControllerDelegate {
    func restaurantDetailViewController(_ viewController: RestaurantDetailViewController, willShow allReviews: AllReviews) {
        let reviewsViewController = ReviewsViewController(allReviews: allReviews)
        navigationController.pushViewController(reviewsViewController, animated: true)
    }
    
    func restaurantDetailViewController(_ viewController: RestaurantDetailViewController, photoElements: [PhotoElement], selected index: Int) {
        let galleryViewController = GalleryViewController(photoElements: photoElements, isPagingEnabled: true)
        galleryViewController.selectedIndex = index
        navigationController.pushViewController(galleryViewController, animated: true)
    }
}

// MARK: - Searchable
// MARK: -
extension Coordinator: Searchable {
    func viewControllerWillGoToSearch(_ viewController: UIViewController) {
        let searchViewController = SearchViewController(networkClient: networkClient)
        searchViewController.delegate = self
        navigationController.pushViewController(searchViewController, animated: true)
    }
}

// MARK: - RestaurantDetailViewControllerDelegate
// MARK: -
extension Coordinator: SearchViewControllerDelegate {
    func searchViewController(_ viewController: SearchViewController, didSelect restaurant: Restaurant) {
        let restaurantDetailViewController = RestaurantDetailViewController(restaurant: restaurant, networkClient: networkClient)
        restaurantDetailViewController.delegate = self
        navigationController.pushViewController(restaurantDetailViewController, animated: true)
    }
}
