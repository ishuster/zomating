//
//  AppDelegate.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    lazy var coordinator: Coordinator = {
        let navigationController = UINavigationController()
        let networkClient = NetworkClient(urlSession: URLSession.shared)
        return Coordinator(navigationController: navigationController, networkClient: networkClient)
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = coordinator.navigationController
        window.makeKeyAndVisible()
        self.window = window
        
        coordinator.start()
        
        return true
    }

}
