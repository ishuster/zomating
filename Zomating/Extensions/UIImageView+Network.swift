//
//  UIImageView+Network.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Kingfisher
import UIKit

extension UIImageView {
    func loadImage(urlString: String) {
        kf.indicatorType = .activity
        kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "empty-icon"))
    }
}
