//
//  Array+Autolayout.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension Array where Element == NSLayoutConstraint {
    
    /// Activates each constraint in the specified array.
    func activate() {
        NSLayoutConstraint.activate(self)
    }
    
    /// Deactivates each constraint in the specified array.
    func deactivate() {
        NSLayoutConstraint.deactivate(self)
    }
    
    /// Actovate or Deactive the constraint.
    ///
    /// - Parameter isActive: The active state of the constraint.
    func setActive(_ isActive: Bool) {
        if isActive {
            NSLayoutConstraint.activate(self)
        } else {
            NSLayoutConstraint.deactivate(self)
        }
    }
}
