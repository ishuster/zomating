//
//  UIView+Autolayout.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Edges
    
    /// This method is used to configure the relationship between user interface object to the edges that must be satisfied by the constraint-based layout system.
    ///
    /// - Parameters:
    ///   - insets: Edge inset values are applied to a rectangle to shrink or expand the area represented by that rectangle.
    ///   - safeArea: Use safe areas as an aid to laying out your content.
    ///   - activate: The active state of the constraint.
    /// - Returns: An array of NSLayoutConstraint.
    @discardableResult
    func pinToEdges(insets: UIEdgeInsets = .zero, safeArea: Bool = false, activate: Bool = false) -> [NSLayoutConstraint] {
        guard superview != nil else { fatalError("Superview is needed for autolayout. No superview found.") }
        translatesAutoresizingMaskIntoConstraints = false
        
        // TODO: Add `safeArea` anchors.
        let constraints = [leftToSuperView(offset: insets.left, activate: activate),
                           rightToSuperView(offset: insets.right, activate: activate),
                           topToSuperview(offset: insets.top, safeArea: safeArea, activate: activate),
                           bottomToSuperview(offset: insets.bottom, activate: activate)]
        
        return constraints
    }
    
    // MARK: - Right
    
    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the trailing edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func right(to view: UIView,
               anchor: NSLayoutXAxisAnchor? = nil,
               offset: CGFloat = 0,
               activate: Bool = false) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: anchor ?? view.rightAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func rightToLeft(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: view.leftAnchor, offset: -offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func rightToSuperView(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        
        return makeConstraint(from: rightAnchor, equalTo: superview.rightAnchor, offset: offset, activate: activate)
        
    }
    
    // MARK: - Left
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the leading edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func left(to view: UIView,
              anchor: NSLayoutXAxisAnchor? = nil,
              offset: CGFloat = 0,
              activate: Bool = false) -> NSLayoutConstraint {
        let _anchor = anchor ?? view.leftAnchor
        return makeConstraint(from: leftAnchor, equalTo: _anchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left to right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func leftToRight(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: leftAnchor, equalTo: view.rightAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left to right of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func leftToSuperView(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: leftAnchor, equalTo: superview.leftAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Top
    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the top edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - safeArea: Use safe areas as an aid to laying out your content.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func top(to view: UIView,
             anchor: NSLayoutYAxisAnchor? = nil,
             offset: CGFloat = 0,
             safeArea: Bool = false,
             activate: Bool = false) -> NSLayoutConstraint {
        
        let neighborAnchor: NSLayoutYAxisAnchor
        if safeArea {
            neighborAnchor = anchor ?? view.safeAreaLayoutGuide.topAnchor
        } else {
            neighborAnchor = anchor ?? view.topAnchor
        }
        
        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top to bottom of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func topToBottom(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: topAnchor, equalTo: view.bottomAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - safeArea: Use safe areas as an aid to laying out your content.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func topToSuperview(offset: CGFloat = 0, safeArea: Bool = false, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        
        let neighborAnchor: NSLayoutYAxisAnchor = safeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
        
        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Bottom
    
    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the bottom edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottom(to view: UIView,
                anchor: NSLayoutYAxisAnchor? = nil,
                offset: CGFloat = 0,
                activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: anchor ?? view.bottomAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom to top of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottomToTop(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: view.topAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottomToSuperview(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: bottomAnchor, equalTo: superview.bottomAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Width
    
    /// This method is used to configure the layout anchor representing the width of the view's frame.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - multiplier: Defines the anchor's size attribute as equal to the specified anchor multiplied by the constant.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func width(to view: UIView, multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        let constraint = widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier)
        constraint.isActive = activate
        return constraint
    }
    
    /// This method is used to configure the layout anchor representing the height of the view's frame based on it's super view.
    ///
    /// - Parameters:
    ///   - multiplier: Defines the anchor's size attribute as equal to the specified anchor multiplied by the constant.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func heightToSuperView(multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        
        let constraint = heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: multiplier)
        constraint.isActive = activate
        return constraint
    }
    
    /// This method is used to configure the layout anchor representing the height of the view's frame.
    ///
    /// - Parameters:
    ///   - equalToConstant: Defines a constant size for the anchor's size attribute.
    ///   - multiplier: Defines the anchor's size attribute as equal to the specified anchor multiplied by the constant.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func height(equalToConstant: CGFloat = 0, multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        let constraint = heightAnchor.constraint(equalToConstant: equalToConstant)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - Center
    
    /// This method is used to represent the layout anchors for the horizontal and vertical center of the view's frame.
    ///
    /// - Parameter view: UIView Object that will be used as base to set your object to the right.
    @discardableResult
    func center(of view: UIView) -> (xConstraint: NSLayoutConstraint, yConstraint: NSLayoutConstraint) {
        let xConstraint = centerX(of: view)
        let yConstraint = centerY(of: view)
        return (xConstraint, yConstraint)
    }
    
    /// This method is used to represent the layout anchors for the horizontal center of the view's frame.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func centerX(of view: UIView, offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: centerXAnchor, equalTo: view.centerXAnchor, offset: offset, activate: activate)
    }
    
    /// This method is used to represent the layout anchors for the vertical center of the view's frame.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func centerY(of view: UIView, offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: centerYAnchor, equalTo: view.centerYAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Private
    
    /// This method created a constraint that defines the horizontal position of two item's.
    ///
    /// - Parameters:
    ///   - anchor: Constraint that defines a constant offset.
    ///   - neighborAnchor: Constraint that defines one item's attribute as equal to another item's attribute.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    private func makeConstraint(from anchor: NSLayoutXAxisAnchor,
                                equalTo neighborAnchor: NSLayoutXAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        translatesAutoresizingMaskIntoConstraints = false
        return constraint
    }
    
    /// This method created a constraint that defines the vertical position of two item's.
    ///
    /// - Parameters:
    ///   - anchor: Constraint that defines a constant offset.
    ///   - neighborAnchor: Constraint that defines one item's attribute as equal to another item's attribute.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    private func makeConstraint(from anchor: NSLayoutYAxisAnchor,
                                equalTo neighborAnchor: NSLayoutYAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        translatesAutoresizingMaskIntoConstraints = false
        return constraint
    }
    
}
