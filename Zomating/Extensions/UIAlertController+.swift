//
//  UIAlertController+.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIAlertController {
    convenience init(title: String, message: String, handler: @escaping (UIAlertAction) -> Void) {
        self.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        addAction(okAction)
    }
}
