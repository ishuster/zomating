//
//  UIColor+App.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIColor {
    /// System table view background color.
    static var systemGray: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    }
}
