//
//  UILabel+Factory.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UILabel {
    static func create(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.defaultConfiguration()
        return label
    }
    
    func defaultConfiguration() {
        translatesAutoresizingMaskIntoConstraints = false
        textColor = .black
        font = UIFont.systemFont(ofSize: 15)
        numberOfLines = 5
        adjustsFontSizeToFitWidth = true
    }
}
