//
//  Category.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

// MARK: - Categories
struct CategoriesWrapper: Codable {
    let categories: [CategoryResponse]
}

// MARK: - Category
struct CategoryResponse: Codable {
    let categories: Category
}

// MARK: - CategoriesClass
struct Category: Codable {
    let id: Int
    let name: String
}
