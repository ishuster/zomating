//
//  SearchByCategory.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

// MARK: - SearchByCategory
struct SearchByCategory: Codable {
    let resultsFound, resultsStart, resultsShown: Int
    let restaurants: [RestaurantElement]
    
    enum CodingKeys: String, CodingKey {
        case resultsFound = "results_found"
        case resultsStart = "results_start"
        case resultsShown = "results_shown"
        case restaurants
    }
}

// MARK: - RestaurantElement
struct RestaurantElement: Codable {
    let restaurant: Restaurant
}

// MARK: - RestaurantRestaurant
struct Restaurant: Codable {
    let id: String
    let name: String
    let url: String
    let location: Location
    let cuisines: String
    let timings: String
    let thumb: String
    let userRating: UserRating
    let allReviewsCount: Int
    let photosURL: String
    let photoCount: Int
    let photos: [PhotoElement]?
    let featuredImage: String
    let allReviews: AllReviews
    
    enum CodingKeys: String, CodingKey {
        case id, name, url, location
        case cuisines, timings
        case thumb
        case userRating = "user_rating"
        case allReviewsCount = "all_reviews_count"
        case photosURL = "photos_url"
        case photoCount = "photo_count"
        case photos
        case featuredImage = "featured_image"
        case allReviews = "all_reviews"
    }
}

// MARK: - AllReviews
struct AllReviews: Codable {
    let reviews: [ReviewElement]
}

// MARK: - ReviewElement
struct ReviewElement: Codable {
    let review: Review
}

// MARK: - ReviewReview
struct Review: Codable {
    let rating: Double
    let reviewText: String
    let id: Int
    let ratingColor: String
    let reviewTimeFriendly: String
    let ratingText: String
    let timestamp, likes: Int
    let user: User
    let commentsCount: Int
    
    enum CodingKeys: String, CodingKey {
        case rating
        case reviewText = "review_text"
        case id
        case ratingColor = "rating_color"
        case reviewTimeFriendly = "review_time_friendly"
        case ratingText = "rating_text"
        case timestamp, likes, user
        case commentsCount = "comments_count"
    }
}

// MARK: - User
struct User: Codable {
    let name: String
    let foodieLevel: String
    let foodieLevelNum: Int
    let foodieColor: String
    let profileURL: String
    let profileImage: String
    let profileDeeplink: String
    let zomatoHandle: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case foodieLevel = "foodie_level"
        case foodieLevelNum = "foodie_level_num"
        case foodieColor = "foodie_color"
        case profileURL = "profile_url"
        case profileImage = "profile_image"
        case profileDeeplink = "profile_deeplink"
        case zomatoHandle = "zomato_handle"
    }
}

// MARK: - Location
struct Location: Codable {
    let address, locality, city: String
    let cityID: Int
    let latitude, longitude, zipcode: String
    let countryID: Int
    let localityVerbose: String
    
    enum CodingKeys: String, CodingKey {
        case address, locality, city
        case cityID = "city_id"
        case latitude, longitude, zipcode
        case countryID = "country_id"
        case localityVerbose = "locality_verbose"
    }
}

// MARK: - PhotoElement
struct PhotoElement: Codable {
    let photo: Photo
}

// MARK: - PhotoPhoto
struct Photo: Codable {
    let id: String
    let url, thumbURL: String
    let user: User
    let resID: Int
    let caption: String
    let timestamp: Int
    let friendlyTime: String
    let width, height: Int
    
    enum CodingKeys: String, CodingKey {
        case id, url
        case thumbURL = "thumb_url"
        case user
        case resID = "res_id"
        case caption, timestamp
        case friendlyTime = "friendly_time"
        case width, height
    }
}

// MARK: - HasMenuStatus
struct HasMenuStatus: Codable {
    let delivery, takeaway: Int
}

// MARK: - UserRating
struct UserRating: Codable {
    let aggregateRating: String?
    let ratingText: String
    let ratingColor: String
    let ratingObj: RatingObj?
    let votes: String
    
    enum CodingKeys: String, CodingKey {
        case aggregateRating = "aggregate_rating"
        case ratingText = "rating_text"
        case ratingColor = "rating_color"
        case ratingObj = "rating_obj"
        case votes
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let aggregateRatingString = try? container.decodeIfPresent(String.self, forKey: .aggregateRating) {
            aggregateRating = "\(aggregateRatingString)"
        } else if let aggregateRatingNumber = try? container.decodeIfPresent(Double.self, forKey: .aggregateRating) {
            aggregateRating = "\(aggregateRatingNumber)"
        } else {
            aggregateRating = try container.decodeIfPresent(String.self, forKey: .aggregateRating)
        }
        
        if let votesString = try? container.decodeIfPresent(String.self, forKey: .votes) {
            votes = votesString
        } else if let votesInt = try? container.decodeIfPresent(Int.self, forKey: .votes) {
            votes = "\(votesInt)"
        } else {
            votes = try container.decode(String.self, forKey: .votes)
        }
        
        let ratingText = try container.decodeIfPresent(String.self, forKey: .ratingText) ?? ""
        let ratingColor = try container.decodeIfPresent(String.self, forKey: .ratingColor) ?? ""
        let ratingObj = try container.decodeIfPresent(RatingObj.self, forKey: .ratingObj)
        
        self.ratingText = ratingText
        self.ratingColor = ratingColor
        self.ratingObj = ratingObj
    }
}

// MARK: - RatingObj
struct RatingObj: Codable {
    let title: Title
    let bgColor: BgColor
    
    enum CodingKeys: String, CodingKey {
        case title
        case bgColor = "bg_color"
    }
}

// MARK: - BgColor
struct BgColor: Codable {
    let type: String
    let tint: String
}

// MARK: - Title
struct Title: Codable {
    let text: String
}
