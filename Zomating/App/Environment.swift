//
//  Environment.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum Environment {
    case debug
    case release
    
    static var current: Environment {
        #if DEBUG
        return .debug
        #else
        return .release
        #endif
    }
    static var baseURLString: String {
        switch current {
        case .debug:
            return "https://developers.zomato.com/api/v2.1"
            
        case .release:
            // TODO: Replace production URL.
            return "https://developers.zomato.com/api/v2.1"
        }
    }
    
    static var userKey: String {
        return "68695bc3d4ad6b9756b8bd057eb6faff"
    }
}
