//
//  EmptyView.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol EmptyViewDelegate: AnyObject {
    func emptyView(_ view: EmptyView, didSelect retryButton: UIButton)
}

final class EmptyView: UIView {

    weak var delegate: EmptyViewDelegate?
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "empty-icon")
        return imageView
    }()
    
    let label: Label = {
        let label = Label(insets: .label, text: "There's nothing to show")
        return label
    }()
    
    let retryButton: UIButton = {
        let button = UIButton()
        button.setTitle("Retry?", for: .normal)
        button.setTitleColor(.black, for: .normal)
        return button
    }()
    
    private let stackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .systemGray
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true

        stackView.axis = .vertical
        [imageView, label, retryButton].forEach { stackView.addArrangedSubview($0) }
        
        retryButton.addTarget(self, action: #selector(retryButtonTouchUpInside(_:)), for: .touchUpInside)
    }
    
    @objc private func retryButtonTouchUpInside(_ sender: UIButton) {
        delegate?.emptyView(self, didSelect: sender)
    }
    
}
