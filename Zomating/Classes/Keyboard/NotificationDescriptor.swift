//
//  NotificationDescriptor.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct NotificationDescriptor<A> {
    /// Name of the notification
    let name: Foundation.Notification.Name
    
    /// Method to convert notification.userInfo to desired type
    let convert: (Foundation.Notification) -> A
}
