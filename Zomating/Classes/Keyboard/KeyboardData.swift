//
//  KeyboardData.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

struct KeyboardData {
    let beginFrame: CGRect
    let endFrame: CGRect
    let curve: UIView.AnimationCurve? //If we want to use it to synchronize our animation
    let duration: TimeInterval
}

extension KeyboardData {
    init(notification: Foundation.Notification) {
        guard let userInfo = notification.userInfo else {
            self.beginFrame = CGRect.zero
            self.endFrame = CGRect.zero
            self.curve = nil
            self.duration = 0
            
            return
        }
        
        self.beginFrame = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect ?? CGRect.zero
        self.endFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
        self.curve = UIView.AnimationCurve(rawValue: (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int) ?? 0)
        self.duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
    }
}
