//
//  SystemNotification.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

struct SystemNotification {
    static let keyboardShowNotification = NotificationDescriptor<KeyboardData>(name: UIResponder.keyboardWillShowNotification, convert: KeyboardData.init)
    static let keyboardHideNotification = NotificationDescriptor<KeyboardData>(name: UIResponder.keyboardWillHideNotification, convert: KeyboardData.init)
}
