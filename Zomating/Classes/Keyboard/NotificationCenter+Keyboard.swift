//
//  NotificationCenter+Keyboard.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

extension NotificationCenter {
    func addObserver<A>(forDescriptor d: NotificationDescriptor<A>, using block: @escaping (A) -> Void) -> Token {
        let t = addObserver(forName: d.name, object: nil, queue: nil, using: { notification in
            block(d.convert(notification))
        })
        return Token(token: t, center: self)
    }
}
