//
//  ArrangedScrollView.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/15/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class ArrangedScrollView: UIScrollView {
    
    let stackView: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.axis = .vertical
        stack.spacing = 0
        return stack
    }()
    
    var isKeyboardAware: Bool = false {
        didSet { updateKeyboardObseverStatus() }
    }
    
    var insets: UIEdgeInsets = .zero {
        didSet {
            stackView.layoutMargins = insets
        }
    }
    
    private var keyboardShowToken: Token?
    private var keyboardHideToken: Token?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.pinToEdges()
        stackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        alwaysBounceVertical = false
        stackView.isLayoutMarginsRelativeArrangement = true
        clipsToBounds = false
        isScrollEnabled = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: All the UIView instances to be added as subviews
    func addArranged(subviews: UIView...) {
        subviews.forEach(stackView.addArrangedSubview)
    }
    
    func addArranged(subviews: [UIView]) {
        subviews.forEach(stackView.addArrangedSubview)
    }
    
    // MARK: Keyboard State
    
    private func updateKeyboardObseverStatus() {
        if isKeyboardAware {
            keyboardShowToken = NotificationCenter.default.addObserver(forDescriptor: SystemNotification.keyboardShowNotification) { [weak self] keyboardData in
                guard let self = self else { return }
                let frameIntersection = self.frame.intersection(keyboardData.endFrame)
                if !frameIntersection.isNull {
                    self.contentInset.bottom = frameIntersection.height + 10
                }
            }
            
            keyboardHideToken = NotificationCenter.default.addObserver(forDescriptor: SystemNotification.keyboardHideNotification) { [weak self] _ in
                guard let self = self else { return }
                self.contentInset.bottom = 0
            }
        } else {
            keyboardShowToken = nil
            keyboardHideToken = nil
        }
    }
    
    func didUpdateLayout() {
        contentSize = stackView.frame.size
    }
}
