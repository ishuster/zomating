//
//  OperationObserver.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol OperationObserver {
    func operationDidFinish(operation: ZomatingOperation)
    func operation(operation: ZomatingOperation, didFinishWithErrors errors: [Error])
}
