//
//  OperationError.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum OperationError: LocalizedError {
    case cancelled
    case invalidUrl
    case badParameters
    case dataIsNil
    case incorrectDataFormat
}
