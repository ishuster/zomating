//
//  BaseViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, Loadable, Emptyable {
    
    lazy var emptyView = EmptyView(frame: .zero)
    var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let selector = #selector(searchTouchUpInside(_:))
        if responds(to: selector) {
            let searchBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: selector)
            navigationItem.rightBarButtonItem = searchBarButton
        }
    }
    
    @objc func searchTouchUpInside(_ sender: Any) {
        
    }
}
