//
//  RepositoryDataSource.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct RepositoryDataSource<T>: Repository {
    var items: [[T]] = []
}
