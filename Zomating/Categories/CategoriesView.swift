//
//  CategoriesView.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class CategoriesView: UIView {

    weak var dataSource: UICollectionViewDataSource? {
        didSet {
            collectionView.dataSource = dataSource
        }
    }
    weak var collectionViewDelegate: UICollectionViewDelegate? {
        didSet {
            collectionView.delegate = collectionViewDelegate
        }
    }
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return view
    }()
    
    convenience init(collectionViewDelegate: UICollectionViewDelegate? = nil, dataSource: UICollectionViewDataSource? = nil) {
        self.init(frame: .zero)
        collectionView.dataSource = dataSource
        collectionView.delegate = collectionViewDelegate
        self.dataSource = dataSource
        self.collectionViewDelegate = collectionViewDelegate
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(collectionView)
        collectionView.frame = bounds
        collectionView.backgroundColor = .systemGray
        collectionView.pinToEdges(safeArea: true).activate()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
    
    func register(cellType type: UICollectionViewCell.Type) {
        collectionView.register(cellType: type)
    }
}
