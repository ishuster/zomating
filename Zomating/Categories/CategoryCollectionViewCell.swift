//
//  CategoryCollectionViewCell.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class CategoryCollectionViewCell: ShadowCellBase {
    
    let titleLabel: UILabel = {
        let label: UILabel = .create(text: "Category")
        label.textAlignment = .center
        return label
    }()
    
    override func setup() {
        super.setup()
        contentView.addSubview(titleLabel)
        titleLabel.pinToEdges().activate()
    }
    
}
