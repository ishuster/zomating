//
//  CategoriesViewController.swift
//  Zomating
//
//  Created by Alejandro Cárdenas on 9/14/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol CategoriesViewControllerDelegate: Searchable {
    func categoriesViewController(_ viewController: CategoriesViewController, didSelect category: Category)
}

final class CategoriesViewController: BaseViewController {
    
    // MARK: - Public Properties
    // MARK: -
    weak var delegate: CategoriesViewControllerDelegate?
    
    // MARK: - Private Properties
    // MARK: -
    private lazy var categoriesView: CategoriesView = {
        let view = CategoriesView(collectionViewDelegate: self, dataSource: self)
        return view
    }()
    
    private var dataSource = RepositoryDataSource<Category>()
    private let networkClient: NetworkClient
    
    // MARK: - Life Cycle
    // MARK: -
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = categoriesView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Categories"
        emptyView.delegate = self
        categoriesView.register(cellType: CategoryCollectionViewCell.self)
        requestCategories()
    }
    
    private func requestCategories() {
        startAnimatingActivityIndicator()
        networkClient.requestCategories { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let categories):
                self.dataSource.update(newItems: categories)
                DispatchQueue.main.async {
                    self.categoriesView.reloadData()
                    self.stopAnimatingActivityIndicator()
                }
                
            case .failure(let error):
                print(error)
                let alertController = UIAlertController(title: "Network Error", message: error.localizedDescription, handler: { (action) in
                    print(action)
                })
                
                DispatchQueue.main.async {
                    self.present(alertController, animated: true)
                }
            }
        }
    }
    
    // MARK: - Actions
    // MARK: -
    override func searchTouchUpInside(_ sender: Any) {
        delegate?.viewControllerWillGoToSearch(self)
    }
}

// MARK: - EmptyViewDelegate
// MARK: -
extension CategoriesViewController: EmptyViewDelegate {
    func emptyView(_ view: EmptyView, didSelect retryButton: UIButton) {
        requestCategories()
    }
}

// MARK: - UICollectionViewDelegate
// MARK: -
extension CategoriesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        guard let category = dataSource.item(at: indexPath) else { return }
        delegate?.categoriesViewController(self, didSelect: category)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
// MARK: -
extension CategoriesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var padding: CGFloat = 40
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = (layout.sectionInset.left + layout.sectionInset.right) * 1.5
        }
        let width = (collectionView.frame.width - padding) / 2
        return CGSize(width: width, height: 100)
    }
}

// MARK: - UICollectionViewDataSource
// MARK: -
extension CategoriesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfItems(forSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueCell(for: indexPath) as CategoryCollectionViewCell
        guard let category = dataSource.item(at: indexPath) else {
            return cell
        }
        
        cell.titleLabel.text = category.name
        
        return cell
    }
}
