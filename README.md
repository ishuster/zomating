# Zomating
https://gitlab.com/ishuster/zomating

## Requirements

- Swift 5
- Xcode 10.3
- Cocoapods 1.7.2

## Run project

We use `bundler` as our gems manager.

- If you don't have `bundler` install run the following command:
```
$ gem install bundler
```

- Install gems
```
$ bundle install
```

- Update pod repositories
```
$ bundle exec pod repo update
```

-  Install pods
```
$ bundle exec pod install
```

After pods has been installed, open `Zomating.xcworkspace`